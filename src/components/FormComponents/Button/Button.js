import { Component } from "react";

class Btn extends Component {
    onBtnClickHandler() {
        console.log("Push this button to send message")
    }
    render () {
        return (
            <div class="row pb-5">
                <div class="col-sm-12">
                    <button id="btn-clear-log" class="btn btn-success" onClick={this.onBtnClickHandler}>Send data</button>
                </div>
            </div>
        )
    }
}

export default Btn;