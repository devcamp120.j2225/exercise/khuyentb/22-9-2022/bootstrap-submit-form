import { Button } from "bootstrap";
import { Component } from "react";
import Btn from "./Button/Button";
import Input from "./Input/Input";
import Select from "./Select/Select";
import Textarea from "./Textarea/Textarea";

class FormComponents extends Component {
    render() {
        return (
            <div className="jumbotron container pt-4" style={{backgroundColor: "#F2F4F4"}}>
                <Input/>
                <Select/>
                <Textarea/>
                <Btn/>
            </div>
        )
    }
}

export default FormComponents ;
