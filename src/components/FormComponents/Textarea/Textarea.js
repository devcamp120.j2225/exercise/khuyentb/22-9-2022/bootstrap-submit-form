import { Component } from "react";

class Textarea extends Component {
    render () {
        return (
            <div >
                <div className="row form-group mt-2">
                    <div className="col-3">
                        <label>Subject</label>
                    </div>
                    <div className="col-6">
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="6"></textarea>
                    </div>
                </div>
            </div>
        )
    }
}

export default Textarea;