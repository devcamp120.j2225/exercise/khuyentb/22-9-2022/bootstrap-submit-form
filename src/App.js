import "bootstrap/dist/css/bootstrap.min.css"
import FormComponents from "./components/FormComponents/FormComponents";
import TittleComponents from "./components/TittleComponents/TittleComponents";
function App() {
  return (
    <div className="App">
      <div className="container">
        <TittleComponents/>
        <FormComponents/>
      </div>
      
    </div>
  );
}

export default App;
